# Use an official Nginx runtime as base image
FROM nginx

# Copy index.html to default Nginx public directory
COPY ./index.html /usr/share/nginx/html/index.html

# Remove default website configs
RUN rm /etc/nginx/conf.d/*

# Copy custom Nginx configuration
COPY ./configs/mysite.conf /etc/nginx/conf.d/

# Expose port 80
EXPOSE 80

# Command to run Nginx in the foreground
CMD ["nginx", "-g", "daemon off;"]

